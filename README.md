# CPP_MATH_APP

Simple app to practice basic math.

Controls:
__Generate__ button will generate 10 new tasks and refresh your board

In type menu you can Check types of tasks you want to generate (you need to press __Generate__ button after you are done with changes)

In amount menu you can choose size of numbers to generate

And lastly check system
__Select system__ will give you number and you must choose one(or all) correct equations from left and drag them to right box.
__Write system__ You have to manualy write the answer into box and then press Check.

*if the app doesnt show images, you have to move them to directory where are u compiling them*