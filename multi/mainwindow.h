#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <filesystem>
#include <QLabel>
#include <QTimer>
#include <QDebug>
#include <QImage>
#include <QPainter>
#include <QPaintEvent>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QDir>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QActionGroup>
#include <QTableWidget>
#include <QTableWidgetItem>

class History{
public:
    std::string task; //9
    std::string yours; //2*3
    bool result; //false

    History(std::string task, std::string yours, bool result){
        this->task = task;
        this->yours = yours;
        this->result = result;
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    void ShowExam();
    ~MainWindow();

    QWidget *centralWidget;
    std::vector<std::string> data;
    QListWidget *submitMenu;
    QListWidget *choiceMenu;
    QTextEdit *submitText;
    QLabel *number;
    QLabel *answer;
    QLabel *score;
    int score_int;
    std::vector<History> history;

    QAction *negative;
    QAction *adding;
    QAction *multi;
    QAction *small;
    QAction *medium;
    QAction *large;
    QAction *checkSelect;
    QAction *checkWrite;

    void checkAnswer();
    void generateData();
    void showScore();

    QString styleSheet ="background-color: #666; color: #f0f0f0";
};

int calculate(std::string task);



#endif // MAINWINDOW_H
