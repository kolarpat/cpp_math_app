#include "mainwindow.h"


/*

todo:
second checking system
number of tasks, negative numbers?

style??

*/

MainWindow::MainWindow(){
    //this->setStyleSheet(this->styleSheet);

    QMenuBar * menuBar = new QMenuBar();
    this->setMenuBar(menuBar);
    this->adding = new QAction("Add/Sub");
    this->adding->setCheckable(true);
    this->adding->setChecked(true);
    this->multi = new QAction("Multi/Divide");
    this->multi->setCheckable(true);
    this->negative = new QAction("Negative");
    this->negative->setCheckable(true);
    QActionGroup *group = new QActionGroup(this);
    group->setExclusive(true);
    this->small = new QAction("Tens");
    this->small->setCheckable(true);
    this->small->setChecked(true);
    this->medium = new QAction("Hundreds");
    this->medium->setCheckable(true);
    this->large = new QAction("Thousands");
    this->large->setCheckable(true);
    group->addAction(this->small);
    group->addAction(this->medium);
    group->addAction(this->large);
    QMenu *menuType = new QMenu("Type");
    QMenu *menuAmount = new QMenu("Amount");
    menuType->addAction(this->adding);
    menuType->addAction(this->multi);
    menuType->addAction(this->negative);
    QAction *generate = new QAction("Generate");
    QActionGroup *groupC = new QActionGroup(this);
    groupC->setExclusive(true);
    QMenu *checkMenu = new QMenu("Check system");
    this->checkSelect = new QAction("Select");
    this->checkSelect->setCheckable(true);
    this->checkSelect->setChecked(true);
    this->checkWrite = new QAction("Write");
    this->checkWrite->setCheckable(true);
    groupC->addAction(this->checkSelect);
    groupC->addAction(this->checkWrite);
    checkMenu->addAction(this->checkSelect);
    checkMenu->addAction(this->checkWrite);
    connect(this->checkSelect, &QAction::triggered, this, &MainWindow::generateData);
    connect(this->checkWrite, &QAction::triggered, this, &MainWindow::generateData);
    connect(generate, &QAction::triggered, this, &MainWindow::generateData);
    menuBar->addAction(generate);
    menuBar->addMenu(menuType);
    menuAmount->addAction(this->small);
    menuAmount->addAction(this->medium);
    menuAmount->addAction(this->large);
    menuBar->addMenu(menuAmount);
    menuBar->addMenu(checkMenu);

    this->score_int = 0;
    this->setWindowTitle("Basic Math");
    this->answer = new QLabel("");
    this->answer->setAlignment(Qt::AlignCenter);
    this->number = new QLabel("NUMBER");
    this->number->setAlignment(Qt::AlignCenter);
    QFont font = this->number->font();
    font.setPointSize(20);
    this->number->setFont(font);
    this->generateData();
    this->resize(600, 400);
    this->show();
}

void MainWindow::ShowExam(){
    if (this->checkWrite->isChecked()){ //change for checkSystem
        this->submitText = new QTextEdit();
        this->submitText->setPlaceholderText("Answer here");
        this->number = new QLabel("NUMBER");
        this->number->setText(QString::fromStdString(this->data[0]));
        this->number->setAlignment(Qt::AlignCenter);
        QFont font = this->number->font();
        font.setPointSize(20);
        this->number->setFont(font);
        //this->answer = new QLabel("");
        //this->answer->setAlignment(Qt::AlignCenter);
        this->centralWidget = new QWidget();
        QVBoxLayout *centralLayout = new QVBoxLayout();
        this->centralWidget->setLayout(centralLayout);
        centralLayout->addWidget(this->number);
        this->submitText->setFont(font);
        this->submitText->setFocus();
        centralLayout->addWidget(this->submitText);
        QPushButton *check = new QPushButton("Check");
        check->setShortcut(QKeySequence(Qt::Key_Return));
        connect(check, &QPushButton::released, this, &MainWindow::checkAnswer);
        centralLayout->addWidget(check);
        centralLayout->addWidget(this->answer);
        QLabel *remain = new QLabel();
        remain->setText(QString::fromStdString("Remaining: " + std::to_string(this->data.size())));
        centralLayout->addWidget(remain);
        this->setCentralWidget(this->centralWidget);
    }
    else{ //checkselect
        this->number = new QLabel("NUMBER");
        this->number->setAlignment(Qt::AlignCenter);
        QFont font = this->number->font();
        font.setPointSize(20);
        this->number->setFont(font);
        //this->answer = new QLabel("");
        //this->answer->setAlignment(Qt::AlignCenter);
        //std::cout << "start Exam\n" << std::flush;
        this->centralWidget = new QWidget();
        QVBoxLayout *centralLayout = new QVBoxLayout();
        this->centralWidget->setLayout(centralLayout);
        QHBoxLayout *halfLayout = new QHBoxLayout();
        centralLayout->addLayout(halfLayout);
        QVBoxLayout *rightLayout = new QVBoxLayout();
        QVBoxLayout *middleLayout = new QVBoxLayout();
        QVBoxLayout *leftLayout = new QVBoxLayout();
        halfLayout->addLayout(leftLayout);
        halfLayout->addLayout(middleLayout);
        halfLayout->addLayout(rightLayout);
        this->choiceMenu = new QListWidget();
        this->choiceMenu->setDragDropMode(QListView::DragDrop);
        this->choiceMenu->setDefaultDropAction(Qt::MoveAction);
        this->choiceMenu->setSelectionMode(QListView::ExtendedSelection);
        for(auto it = this->data.begin(); it != this->data.end(); it++){
            QListWidgetItem *tmp = new QListWidgetItem(QString::fromStdString(*it));
            tmp->setTextAlignment(Qt::AlignCenter);
            QFont font = tmp->font();
            font.setPointSize(20);
            tmp->setFont(font);
            this->choiceMenu->addItem(tmp);
        }

        leftLayout->addWidget(this->choiceMenu);
        this->score = new QLabel(QString::fromStdString("Score: " + std::to_string(this->score_int)));
        leftLayout->addWidget(this->score);

        QDir dir;
        dir.cd("..");
        std::string path = dir.absolutePath().toStdString() + "/multi/arrow.png";
        QImage image = QImage(path.c_str());
        QLabel *imageLabel = new QLabel();
        imageLabel->setToolTip("Drag answer");
        imageLabel->setPixmap(QPixmap::fromImage(image));
        middleLayout->addWidget(imageLabel);
        //std::cout << "num before" << std::flush;
        rightLayout->addWidget(this->number);
        QList<QListWidgetItem*> items = this->choiceMenu->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);
        this->number->setText(QString::fromStdString(std::to_string(calculate(items[rand() % items.count()]->text().toStdString()))));
        //std::cout << "num after" << std::flush;
        this->submitMenu = new QListWidget();
        this->submitMenu->setDragDropMode(QListView::DragDrop);
        this->submitMenu->setDefaultDropAction(Qt::MoveAction);
        this->submitMenu->setSelectionMode(QListView::ExtendedSelection);
        rightLayout->addWidget(this->submitMenu);
        QPushButton *check = new QPushButton("Check");
        check->setShortcut(QKeySequence(Qt::Key_Return));
        connect(check, &QPushButton::released, this, &MainWindow::checkAnswer);
        rightLayout->addWidget(check);
        rightLayout->addWidget(this->answer);

        this->setCentralWidget(this->centralWidget);
        //std::cout << "end Exam\n" << std::flush;
    }
}

void MainWindow::showScore(){
    //std::cout << "start Score\n" << std::flush;
    this->centralWidget = new QWidget();
    QVBoxLayout *centralLayout = new QVBoxLayout();
    this->centralWidget->setLayout(centralLayout);
    QTableWidget *table = new QTableWidget();
    table->setRowCount(this->history.size());
    table->setColumnCount(2);
    QStringList names;
    names << "task" << "your answer";
    table->setHorizontalHeaderLabels(QStringList(names));
    table->setColumnWidth(0, 200);
    table->setColumnWidth(1, 200);
    int correct = 0;

    for(int i = 0; i < (int)this->history.size(); i++){
        QTableWidgetItem *ans = new QTableWidgetItem(QString::fromStdString(this->history[i].task));
        ans->setTextAlignment(Qt::AlignCenter);
        table->setItem(i, 0, ans);
        ans = new QTableWidgetItem(QString::fromStdString(this->history[i].yours));
        ans->setTextAlignment(Qt::AlignCenter);
        if(this->history[i].result){
            ans->setBackground(Qt::green);
            correct++;

        }
        else{
            ans->setBackground(Qt::red);
        }
        table->setItem(i, 1, ans);
    }
    centralLayout->addWidget(table);
    QPushButton *back = new QPushButton("Again");
    QPushButton *exit = new QPushButton("Exit");
    std::stringstream s;
    s << ((float)correct/this->history.size())*100;
    std::string txt = "Correct: " + s.str() + "%";
    QLabel *percent = new QLabel(QString::fromStdString(txt));
    QHBoxLayout *options = new QHBoxLayout();
    options->addWidget(back);
    options->addWidget(exit);
    centralLayout->addWidget(percent);
    centralLayout->addLayout(options);
    connect(exit, &QPushButton::released, this, &MainWindow::close);
    connect(back, &QPushButton::released, this, &MainWindow::generateData);

    this->setCentralWidget(this->centralWidget);
    this->score = 0;
    this->history.clear();
    //std::cout << "end Score\n" << std::flush;
}

void MainWindow::checkAnswer(){ //change base of check type
    //std::cout << "start answer\n" << std::flush;
    if(this->checkWrite->isChecked()){
        if(calculate(this->number->text().toStdString()) == this->submitText->toPlainText().toInt()){
            this->score++;
            this->history.push_back(History(this->number->text().toStdString(), this->submitText->toPlainText().toStdString(), true));
            this->answer->setText("Correct");
            this->answer->setStyleSheet("Color: Green");
        }
        else{
            this->history.push_back(History(this->number->text().toStdString(), this->submitText->toPlainText().toStdString(), false));
            this->answer->setText("Wrong");
            this->answer->setStyleSheet("Color: Red");
        }
        this->data.erase(this->data.begin());
        this->submitText->setText("");
        if(this->data.size() > 0){
            this->ShowExam();
        }
        else{
            this->showScore();
        }
    }
    else{
        bool correct = true;
        int tempScore = 0;
        for(int i = 0; i < this->submitMenu->count(); i++){
            if(calculate(this->submitMenu->item(i)->text().toStdString()) != std::stoi(this->number->text().toStdString())){
                correct=false;
                this->history.push_back(History(this->number->text().toStdString(), this->submitMenu->item(i)->text().toStdString(), false));
                std::cout << "Wrong\n" << std::flush;
            }
            else{
                tempScore += 1;
                this->history.push_back(History(this->number->text().toStdString(), this->submitMenu->item(i)->text().toStdString(), true));
                std::cout << "Correct\n" << std::flush;
            }
        }
        if(this->submitMenu->count() == 0){
            correct = false;
        }
        if(correct){
            this->answer->setText("Correct");
            this->answer->setStyleSheet("Color: Green");
            this->score_int += tempScore;
        }else{
            this->answer->setText("Wrong");
            this->answer->setStyleSheet("Color: Red");
        }
        this->submitMenu->clear();
        try {
            for(auto it = this->data.begin(); it != this->data.end(); it++){
                if(calculate(*it) == std::stoi(this->number->text().toStdString())){
                    this->data.erase(it);
                }
            }
        } catch (...) {
        }
        //std::cout << this->choiceMenu->count();
        if (this->choiceMenu->count() == 0){ //change for checkSystem
            this->showScore();
        }
        else{
            QList<QListWidgetItem*> items = this->choiceMenu->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);
            this->number->setText(QString::fromStdString(std::to_string(calculate(items[rand() % items.count()]->text().toStdString()))));
            this->ShowExam();
        }
    //std::cout << "end answer\n" << std::flush;
    }
}

void MainWindow::generateData(){ //done
    //std::cout << "start gen\n" << std::flush;
    this->data.clear();

    srand((unsigned) time(NULL));
    std::vector<std::string> possibles;
    if(this->adding->isChecked()){
        possibles.push_back("+");
        possibles.push_back("-");
    }
    if(this->multi->isChecked()){
        possibles.push_back("x");
        possibles.push_back("/");
    }
    if(possibles.size() == 0){
        possibles.push_back("+");
    }
    int constant = 10;
    if(this->medium->isChecked()){
        constant = 100;
    }
    if(this->large->isChecked()){
        constant = 1000;
    }
    //std::cout << constant << " ... " << std::to_string(possibles.size()) + "\n";
    while(this->data.size() < 10){
        std::string tmpF = std::to_string(((int)rand() % constant));
        std::string tmpS = std::to_string(((int)rand() % constant));
        int choice = rand()%possibles.size();
        if(possibles[choice] == "+"){
            this->data.push_back(tmpF+"+"+tmpS);
        }
        if(possibles[choice] == "-"){
            if(this->negative->isChecked()){
                this->data.push_back(tmpF+"-"+tmpS);
            }
            else{
                if(tmpF < tmpS){
                    this->data.push_back(tmpS+"-"+tmpF);
                }
                else{
                    this->data.push_back(tmpF+"-"+tmpS);
                }
            }
        }
        if(possibles[choice] == "x"){
            this->data.push_back(tmpF+"x"+tmpS);
        }
        if(possibles[choice] == "/"){
            this->data.push_back(std::to_string(std::stoi(tmpF)*(rand()%3))+"/"+tmpF);
        }
    }
        //std::cout << "generated: " << this->data[this->data.size()-1] << "\n";
    //std::cout << "generated!" << std::flush;
    //std::cout << "setted number (gen)\n" << std::flush;
    this->answer = new QLabel("");
    this->answer->setAlignment(Qt::AlignCenter);
    this->ShowExam();
    //std::cout << "end gen\n" << std::flush;
}

MainWindow::~MainWindow(){

}

int calculate(std::string task){
    /*int tmpF = (((int)task[0])-48);
    int tmpS = (((int)task[2])-48);*/
    int index = task.find_first_of("+-x/");
    int tmpF = std::stoi(task.substr(0, index));
    int tmpS = std::stoi(task.substr(index+1, task.size()-1));
    std::cout << task.substr(0, index) << " ... " << task.substr(index+1, task.size()-1) << "\n";
    if(task[index]=='x')
        return tmpF * tmpS;
    if(task[index]=='/')
        return tmpF / tmpS;
    if(task[index]=='+')
        return tmpF + tmpS;
    if(task[index]=='-')
        return tmpF - tmpS;
    return 0;
}
